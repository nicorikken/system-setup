<!--
SPDX-FileCopyrightText: 2022 Nico Rikken

SPDX-License-Identifier: CC0-1.0
-->

# System setup

Ansible playbook to make my basic system setup repeatable.

I share it so others can draw inspiration from it.
Although it is as generic as possible, it can contain quite specific configurations for my needs.
It targets my main distributions: [Debian](https://www.debian.org/), [Mobian](https://mobian-project.org/), [Ubuntu](https://ubuntu.com/) and [OpenWrt](https://openwrt.org/)
If you intend to use it, please create your own version of it and customize it.

[Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible)
My preferred method is via [pipx](https://pypi.org/project/pipx/)

```
pipx install ansible --include-deps
```

Ensure dependencies:

```
ansible-galaxy install -r requirements.yaml
```

Create an inventory file, which can be created from the `inventory.example.yaml` file:

```
cp inventory.example.yaml inventory.yaml
```

This repository contains multiple playbooks to seperate the different situations.
By default the playbooks are expected to target the host machine.
For other situations specify a different host using the commandline parameters or in the inventory file. 

Check the current installation using the `--check` flag:

```
ansible-playbook --limit local --check pb-personal.yaml
```

Run the playbook to configure the system (without the `--check` flag):

```
ansible-playbook --limit local pb-personal.yaml
```

## License

This project is licensed according to the [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Nico Rikken has waived all copyright and related or neighboring rights to System Setup. This work is published from: Netherlands. 

